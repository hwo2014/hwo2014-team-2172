var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
var trackName = process.argv[6] || null;
var carCount  = parseFloat(process.argv[7] || 1);
var password  = process.argv[8] || null;
var suffix = process.argv[9] || '';
var fastness = parseFloat(process.argv[10] || 1);

console.log(process.argv[10]);

var bot = require('./mybot')(fastness);



console.log("I'm", botName + suffix, "and connect to", serverHost + ":" + serverPort);

var client = net.connect(serverPort, serverHost, function() {

  if (!trackName) {
      send({"msgType": "join", "data": {
          "name": botName,
          "key": botKey
      }});
  }
  else {
      var msg = {
          msgType: "joinRace",
          data: {
            botId: {
              name: botName + suffix,
              key: botKey
            },
            carCount: carCount,
            trackName: trackName,
            password: password
          }
      };
      console.log("Join", msg);
      send(msg);
  }
});

client.on('close', function() {
    console.log("Its the end");
});


function send(json) {
  client.write(JSON.stringify(json));
  client.write('\n');
}

var jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
    var resp = bot(data);
    if (resp) send(resp);
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});


















