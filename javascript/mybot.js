var Handler = (function () {
    function Handler(fastness) {
        if (typeof fastness === "undefined") { fastness = 1; }
        this.fastness = fastness;
        this.lane = null;
        this.turbo = null;
        console.log("Fastness", this.fastness);
    }
    Handler.prototype.yourCar = function (data) {
        this.me = data;
    };
    Handler.prototype.gameInit = function (data) {
        this.track = data.race.track;
        this.cars = data.race.cars;
        this.session = data.race.raceSession;

        for (var k = 0; k < this.track.pieces.length; ++k) {
            this.track.pieces[k].index = k;
        }
        console.log("Race:", JSON.stringify(data, null, 4));

        this.estimator = new Estimator(this.track);
        this.pidRegulator = new PidRegulator(this.track);
        this.overtaker = new Overtaker(this.track, this.cars);
    };
    Handler.prototype.turboAvailable = function (data) {
        console.log("TURBO");
        console.log(data);
        this.turbo = data;
    };
    Handler.prototype.spawn = function (data) {
        // Reset turbo on spawn!
        this.turbo = null;
    };
    Handler.prototype.carPositions = function (data) {
        var _this = this;
        this.carsPos = data;

        this.myPos = data.filter(function (cp) {
            return cp.id.color == _this.me.color;
        })[0];
        var lanes = this.myPos.piecePosition.lane;
        if (this.lane == null)
            this.lane = lanes.startLaneIndex;
        else if (lanes.startLaneIndex != lanes.endLaneIndex)
            this.lane = lanes.endLaneIndex;

        var pieceIndex = this.myPos.piecePosition.pieceIndex;
        var piece = this.track.pieces[pieceIndex];

        this.overtaker.stateUpdate(this.myPos, this.carsPos);

        var throttleR = this.pidRegulator.handle(this.myPos);
        var throttleS = this.estimator.handle(this.myPos);

        var throttle = Math.min(this.fastness, throttleR, throttleS);
        console.log("Lane = ", this.lane);
        console.log("Throttle = ", throttle.toFixed(2));

        var msgs = [];
        if (this.turbo && this.estimator.shouldTurbo(this.myPos)) {
            console.log("Should turbo.");
            this.turbo = null;
            return { msgType: "turbo", data: "lol" };
        }
        var lanesS = this.estimator.getLanesNext(this.myPos);
        var avoidLanes = this.overtaker.getAvoidedLanes();
        if ((lanesS.startLaneIndex != lanesS.endLaneIndex) && (lanesS.endLaneIndex != this.lane)) {
            if (!~avoidLanes.indexOf(lanesS.endLaneIndex)) {
                console.log("Allowed to switch lanes");
                this.lane = lanesS.endLaneIndex;
                return this.getSwitchDirectionMsg(lanesS);
            } else {
                console.log("Forbidden to switch lanes because");
                console.log(avoidLanes, "contains", lanesS.endLaneIndex, "=", !!~avoidLanes.indexOf(lanesS.endLaneIndex));
            }
        } else {
            var suggestion = this.overtaker.switchSuggestion(this.myPos);
            if (suggestion !== this.myPos.piecePosition.lane.endLaneIndex && suggestion !== this.lane) {
                var start = this.lane;
                this.lane = suggestion;
                return this.getSwitchDirectionMsg({
                    startLaneIndex: start,
                    endLaneIndex: this.lane
                });
            }
        }

        return { msgType: "throttle", data: throttle };
    };
    Handler.prototype.getSwitchDirectionMsg = function (lanes) {
        console.log("Switching", lanes);
        var dir = this.track.lanes[lanes.endLaneIndex].distanceFromCenter - this.track.lanes[lanes.startLaneIndex].distanceFromCenter;
        var dirStr = dir > 0 ? "Right" : "Left";
        console.log("Switching with msg", { msgType: "switchLane", data: dirStr });
        return { msgType: "switchLane", data: dirStr };
    };
    return Handler;
})();

var Overtaker = (function () {
    function Overtaker(t, cars) {
        this.t = t;
        this.cars = cars;
        this.mySpeed = 0;
        this.avoidLanes = [];
        this.bumps = cars.map(function (c) {
            return 0;
        });
    }
    Overtaker.prototype.getRadiusLane = function (piece, l) {
        var rModifier = this.t.lanes[l].distanceFromCenter;
        if (piece.angle > 0)
            return piece.radius - rModifier;
        else if (piece.angle < 0)
            return piece.radius + rModifier;
        else
            return piece.radius;
    };
    Overtaker.prototype.getLength = function (piece, lane) {
        //todo estimate based on lane.
        return piece.length || this.getRadiusLane(piece, lane) * Math.abs(piece.angle) * Math.PI / 180.0;
    };
    Overtaker.prototype.getDistance = function (pos, pos1) {
        if (pos.piecePosition.pieceIndex == pos1.piecePosition.pieceIndex)
            return pos.piecePosition.inPieceDistance - pos1.piecePosition.inPieceDistance;
        else {
            var oldPiece = this.t.pieces[pos1.piecePosition.pieceIndex];
            return (this.getLength(oldPiece, pos1.piecePosition.lane.endLaneIndex) - pos1.piecePosition.inPieceDistance) + pos.piecePosition.inPieceDistance;
        }
    };

    Overtaker.prototype.stateUpdate = function (myPos, others) {
        var _this = this;
        others = others.filter(function (cp) {
            return cp.id.color != myPos.id.color;
        });
        this.others = others;
        if (this.oldOthers) {
            this.speeds = this.others.map(function (o, i) {
                return _this.getDistance(o, _this.oldOthers[i]);
            });
            this.mySpeed = this.getDistance(myPos, this.oldPos);

            this.avoidLanes = [];

            others.forEach(function (other, k) {
                var otherMinusMy = other.piecePosition.pieceIndex - myPos.piecePosition.pieceIndex;
                if (otherMinusMy > 2 || otherMinusMy < 0)
                    return;
                if (_this.speeds[k] / _this.mySpeed < 0.9)
                    _this.avoidLanes.push(other.piecePosition.lane.endLaneIndex);
            });
        }
        this.oldOthers = this.others;
        this.oldPos = myPos;
    };
    Overtaker.prototype.switchSuggestion = function (myPos) {
        var _this = this;
        var myLane = myPos.piecePosition.lane.endLaneIndex, suggestions = [myLane];
        var nextItem = (myPos.piecePosition.pieceIndex + 1) % this.t.pieces.length;
        if (!this.t.pieces[nextItem].switch)
            return myLane;

        if (myLane > 0)
            suggestions.push(myLane - 1);
        if (myLane < this.t.lanes.length - 1)
            suggestions.push(myLane + 1);

        var okay = suggestions.filter(function (alt) {
            return _this.avoidLanes.indexOf(alt) < 0;
        });
        if (okay.indexOf(myLane))
            return myLane;
        if (!okay.length)
            return myLane;

        return okay[0];
    };
    Overtaker.prototype.getAvoidedLanes = function () {
        return this.avoidLanes;
    };
    return Overtaker;
})();

// The only remaining idea I have is to turn of Kd if
// the next turn is at approximately the wrong distance.
var PidRegulator = (function () {
    function PidRegulator(t) {
        this.t = t;
        this.previousError = null;
        this.integral = 0;
        this.setPoint = 45;
        this.Kp = 1;
        this.Ki = 0;
        this.Kdp = 10;
        this.Kdn = 6;
    }
    PidRegulator.prototype.handle = function (pos) {
        var p = this.t.pieces[pos.piecePosition.pieceIndex];

        //var offset = 15.0 * Math.max(0, (100 - (p.radius || 100)) / 50) // great on *
        this.setPoint = 15 + (p.radius || 200) / 4.5; // crashes on usa, great on *

        //this.setPoint = Math.sqrt(p.radius || 200) * 4.5;  // again crash on usa
        //this.setPoint = 12.5 + Math.sqrt(p.radius || 200) * 3;  //
        this.setPoint = Math.sqrt(p.radius || 200) * 3.8; // 3.9 worked fine

        var angle = pos.angle;
        var error = (this.setPoint - Math.abs(angle)) / 30.0;
        if (this.previousError == null)
            this.previousError = error;
        this.integral = this.integral + error;
        if (this.integral > 0)
            this.integral = 0;

        // Since the error goes negative when the angle grows
        // that means the derivative is smaller - larger < 0
        // therefore Kd should be positive
        var derivative = error - this.previousError;

        var Kd = derivative > 0 ? this.Kdp : this.Kdn;
        var output = 0.5 + this.Kp * error + this.Ki * this.integral + derivative * Kd;
        this.previousError = error;
        console.log("A=", angle.toFixed(1), "E=", error.toFixed(2), "O=", output.toFixed(2));

        return Math.min(1, Math.max(0, output));
    };
    return PidRegulator;
})();

var Estimator = (function () {
    function Estimator(t) {
        this.t = t;
        this.oldSpeed = 0;
        this.oldAngle = 0;
        this.v1 = 0;
        this.v2 = 0;
        this.v3 = 0;
        this.m = 0;
        this.k = 0;
        this.pickedLanes = t.pieces.map(function (p) {
            return ({ startLaneIndex: 0, endLaneIndex: 0 });
        });
    }
    Estimator.prototype.pickLanes = function (pos) {
        var _this = this;
        var currentLaneIndex = pos.piecePosition.lane.startLaneIndex;
        var currentLane = this.t.lanes[currentLaneIndex];

        this.takePieceWhileF(pos.piecePosition.pieceIndex, function (p) {
            _this.pickedLanes[p.index].startLaneIndex = currentLaneIndex;
            if (!p.switch) {
                _this.pickedLanes[p.index].endLaneIndex = currentLaneIndex;
            } else {
                var laneOptions = [];
                if (currentLaneIndex != 0)
                    laneOptions.push(currentLaneIndex - 1);
                laneOptions.push(currentLaneIndex);
                if (currentLaneIndex < _this.t.lanes.length - 1)
                    laneOptions.push(currentLaneIndex + 1);

                var choices = laneOptions.map(function (lane) {
                    return ({
                        lane: lane,
                        len: _this.takePieceWhileF(p.index, function (np) {
                            return np.index == p.index || !np.switch;
                        }).items.map(function (p) {
                            return _this.getLengthLane(p, lane);
                        }).reduce(function (l, acc) {
                            return l + acc;
                        }, 0)
                    });
                });
                var best = choices.sort(function (left, right) {
                    return left.len - right.len;
                })[0].lane;
                _this.pickedLanes[p.index].endLaneIndex = best;
                currentLaneIndex = best;
            }
            if (_this.nextPid(p.index) == pos.piecePosition.pieceIndex)
                return false;
            return true;
        });
    };

    Estimator.prototype.shouldTurbo = function (pos) {
        var total = this.takePieceWhileF(pos.piecePosition.pieceIndex, function (p) {
            return !p.angle && !p.radius;
        }).items.map(function (p) {
            return p.length;
        }).reduce(function (acc, el) {
            return acc + el;
        }, 0);
        console.log("Straight ahead", total);
        if (total > 450)
            return true;
        return false;
    };

    Estimator.prototype.getLanesNext = function (pos) {
        return this.pickedLanes[this.nextPid(pos.piecePosition.pieceIndex)];
    };

    Estimator.prototype.takePieceWhile = function (pId, direction, predicate) {
        var piece = this.t.pieces[pId], all = [];
        while (predicate(piece)) {
            all.push(piece);
            pId = direction(pId);
            piece = this.t.pieces[pId];
        }
        return { nextPid: pId, items: all };
    };

    Estimator.prototype.takePieceWhileF = function (pId, predicate) {
        var _this = this;
        return this.takePieceWhile(pId, function (pid) {
            return _this.nextPid(pid);
        }, predicate);
    };
    Estimator.prototype.takePieceWhileR = function (pId, predicate) {
        var _this = this;
        return this.takePieceWhile(pId, function (pid) {
            return _this.prevPid(pid);
        }, predicate);
    };

    Estimator.prototype.getTurnDirection = function (piece) {
        if (!piece.angle)
            return 0;
        else
            return piece.angle > 0 ? 1 : -1;
    };

    Estimator.prototype.getRadiusLane = function (piece, l) {
        var rModifier = this.t.lanes[l].distanceFromCenter;
        if (piece.angle > 0)
            return piece.radius - rModifier;
        else if (piece.angle < 0)
            return piece.radius + rModifier;
        else
            return piece.radius;
    };
    Estimator.prototype.getRadius = function (piece) {
        var lanes = this.pickedLanes[piece.index];
        return Math.min(this.getRadiusLane(piece, lanes.startLaneIndex), this.getRadiusLane(piece, lanes.endLaneIndex));
    };
    Estimator.prototype.getLengthLane = function (piece, l) {
        //todo estimate based on lane.
        return piece.length || this.getRadiusLane(piece, l) * Math.abs(piece.angle) * Math.PI / 180.0;
    };
    Estimator.prototype.getLength = function (piece) {
        //todo estimate based on lane.
        return piece.length || this.getRadius(piece) * Math.abs(piece.angle) * Math.PI / 180.0;
    };
    Estimator.prototype.nextPid = function (pid) {
        var next = (pid + 1);
        if (next >= this.t.pieces.length)
            return 0;
        return next;
    };
    Estimator.prototype.prevPid = function (pid) {
        var prev = (pid - 1);
        if (prev < 0)
            return this.t.pieces.length - 1;
        return prev;
    };
    Estimator.prototype.percentualDifference = function (n1, n2) {
        return Math.max(n1, n2) / Math.min(n1, n2) - 1;
    };
    Estimator.prototype.getAvailableBreakingDistance = function (pos) {
        var _this = this;
        var pid = pos.piecePosition.pieceIndex, piece = this.t.pieces[pid];
        var len = this.getLength(piece);
        var startTD = this.getTurnDirection(piece), startRadius = this.getRadius(piece);
        var thisRemaining = len - pos.piecePosition.inPieceDistance;
        var nextPid = this.nextPid(pid);
        var currentPieces = this.takePieceWhileF(nextPid, function (pc) {
            return startTD && _this.getTurnDirection(pc) == startTD && _this.percentualDifference(pc.radius, startRadius) < 0.1;
        });

        var straightPieces = this.takePieceWhileF(currentPieces.nextPid, function (pc) {
            return !_this.getTurnDirection(pc);
        });

        //console.log("Pieces to next turn: ", currentPieces.items, straightPieces.items);
        var endPid = straightPieces.nextPid;
        var reducer = function (l, piece) {
            return l + _this.getLength(piece);
        };
        var curLen = currentPieces.items.reduce(reducer, 0);
        var straightLen = straightPieces.items.reduce(reducer, 0);
        return { nextPid: endPid, length: thisRemaining + curLen + straightLen };
    };
    Estimator.prototype.estimateDesiredSpeed = function (pid, speed) {
        //var MAXLEN = 250; // this value worked REALLY well. with 0.6
        //var K = 0.65; // this worked really well with 200
        var _this = this;
        var MAXLEN = 160;
        var K = 0.66;

        var piece = this.t.pieces[pid];
        var turnDirection = this.getTurnDirection(piece);
        var d = 0.0, turnAmount = 0.0, angleAmount = 0.0;
        var turnPieces = this.takePieceWhileF(pid, function (piece) {
            var pLen = _this.getLength(piece);

            var take = MAXLEN - d;
            if (take > pLen)
                take = pLen;
            if (piece.angle) {
                var anglePerc = (Math.abs(piece.angle) * take / pLen);
                angleAmount += anglePerc;
                turnAmount += anglePerc / Math.sqrt(_this.getRadius(piece));
            }
            if ((d += pLen) > MAXLEN)
                return false;
            return true;
        });

        //console.log("Turn pieces", turnPieces);
        return K / (turnAmount / angleAmount);
    };
    Estimator.prototype.getDistance = function (pos) {
        if (!this.oldPos)
            return 0;
        if (pos.piecePosition.pieceIndex == this.oldPos.piecePosition.pieceIndex)
            return this.oldSpeed = pos.piecePosition.inPieceDistance - this.oldPos.piecePosition.inPieceDistance;
        else {
            var oldPiece = this.t.pieces[this.oldPos.piecePosition.pieceIndex];
            return (this.getLength(oldPiece) - this.oldPos.piecePosition.inPieceDistance) + pos.piecePosition.inPieceDistance;
        }
    };
    Estimator.prototype.setSpeeds = function (speed) {
        if (!this.v1)
            this.v1 = speed;
        else if (!this.v2)
            this.v2 = speed;
        else if (!this.v3) {
            this.v3 = speed;

            // k = ( v(1) - ( v(2) - v(1) ) ) / v(1)2 * h;
            this.k = (this.v1 - (this.v2 - this.v1)) / (this.v1 * this.v1);
            this.m = 1.0 / (Math.log((this.v3 - (1 / this.k)) / (this.v2 - (1 / this.k))) / (0 - this.k));
            console.log("===");
            console.log("Speed", this.v1.toFixed(3));
            console.log("K=", this.k.toFixed(3), "M=", this.m.toFixed(3));
            console.log("===");
        }
    };

    Estimator.prototype.getNecessaryBreakingTicks = function (speed, desiredSpeed) {
        return Math.log(desiredSpeed / speed) * this.m / (0 - this.k);
    };
    Estimator.prototype.getBreakingDistance = function (speed, ticks) {
        return (this.m / this.k) * speed * (1.0 - Math.exp((0 - this.k) * ticks / this.m));
    };
    Estimator.prototype.handle = function (pos) {
        if (!this.oldPos) {
            this.oldPos = pos;
            return 1;
        }
        var speed = this.getDistance(pos);
        this.setSpeeds(speed);
        this.oldPos = pos;
        if (!this.m)
            return 1;

        console.log("At", this.t.pieces[pos.piecePosition.pieceIndex]);
        this.pickLanes(pos);
        var availableDistance = this.getAvailableBreakingDistance(pos);
        var desiredSpeed = this.estimateDesiredSpeed(availableDistance.nextPid, speed);
        var necessaryFrames = this.getNecessaryBreakingTicks(speed, desiredSpeed);
        var necessaryDistance = this.getBreakingDistance(speed, necessaryFrames);

        console.log("Speed", speed.toFixed(2), "Desired", desiredSpeed.toFixed(2), "BreakDistanceAvailable", availableDistance.length.toFixed(1), "BreakDistanceNecessary", necessaryDistance.toFixed(1));

        if (necessaryDistance > availableDistance.length)
            return 0;
        else
            return 1;

        var availableFrames = availableDistance.length / speed;

        var toTheEnd = 0.15 * availableFrames;

        console.log("Speed", speed.toFixed(2), "Desired", desiredSpeed.toFixed(2), "AvailableFrames", availableFrames.toFixed(1));

        if (speed - desiredSpeed > Math.max(toTheEnd, 0))
            return 0;
        else
            return 1;
        return 0.5;
    };
    return Estimator;
})();

var ping = { msgType: "ping", data: {} };

function create(fastness) {
    var handler = new Handler(fastness);
    return function (data) {
        if (handler[data.msgType])
            return handler[data.msgType](data.data) || ping;
        else {
            console.log("Server:", data);
            return ping;
        }
    };
}

module.exports = create;
