
interface CarId {
    name: string;
    color: string;
}
interface CarDimensions {
    length: number;
    width: number;
    guideFlagPosition: number;
}
interface Car {
    id: CarId;
    dimensions: CarDimensions;
}

interface Session {
    laps: number;
    maxLapTimeMs: number;
    quickRace: boolean;
}

interface Piece {
    index: number;
    length?: number;
    switch?: boolean;
    radius?: number;
    angle?:number;
}
interface Lane {
    index: number;
    distanceFromCenter: number;
}
interface Point {
    x: number;
    y: number;
}

interface Track {
    id: string;
    name: string;
    pieces: Piece[];
    lanes: Lane[];
    startingPoint: any;
}

interface GameInitData {
    race: {
        track: Track;
        cars: Car[];
        raceSession: Session;        
    };
}

interface PieceLanes {
    startLaneIndex: number;
    endLaneIndex: number;
}

interface CarPosition {
    id: CarId;
    angle: number;
    piecePosition: {
        pieceIndex: number;
        inPieceDistance: number;
        lane: PieceLanes;
        lap: number;
    }
}



interface IEstimator {
    handle(pos:CarPosition):number;
    getLanesNext(pos: CarPosition): PieceLanes;
    shouldTurbo(pos:CarPosition): boolean;
}

interface TurboData {
    turboDurationMilliseconds: number;
    turboDurationTicks: number;
    turboFactor: number;
}

class Handler {
    private track:Track;
    private cars: Car[];
    private carsPos: CarPosition[];
    private lanes: Lane[];
    private session: Session;
    private me: CarId;
    private myPos: CarPosition;
    private lane: number = null;

    private pidRegulator: PidRegulator;
    private estimator: IEstimator;
    private overtaker: Overtaker;
    constructor(private fastness: number = 1) {
        console.log("Fastness", this.fastness);
    }
    
    private turbo: TurboData = null;
    
    public yourCar(data: CarId) {
        this.me = data;
    }
    public gameInit(data:GameInitData) {
        this.track = data.race.track;
        this.cars = data.race.cars;
        this.session = data.race.raceSession;
        
        for (var k = 0; k < this.track.pieces.length; ++k) {
            this.track.pieces[k].index = k;
        }
        console.log("Race:", JSON.stringify(data, null, 4));
        
        this.estimator = new Estimator(this.track);
        this.pidRegulator = new PidRegulator(this.track);
        this.overtaker = new Overtaker(this.track, this.cars);

    }
    public turboAvailable(data: TurboData) {
        console.log("TURBO");
        console.log(data);
        this.turbo = data;
    }
    public spawn(data) {
        // Reset turbo on spawn!
        this.turbo = null;
    }
    public carPositions(data: CarPosition[]):any {
        this.carsPos = data;

        this.myPos = data.filter(cp =>
            cp.id.color == this.me.color)[0];
        var lanes = this.myPos.piecePosition.lane;
        if (this.lane == null)
            this.lane = lanes.startLaneIndex;
        else if (lanes.startLaneIndex != lanes.endLaneIndex)
            this.lane = lanes.endLaneIndex;
        
        var pieceIndex = this.myPos.piecePosition.pieceIndex;
        var piece = this.track.pieces[pieceIndex];

        this.overtaker.stateUpdate(this.myPos, this.carsPos);
        
        var throttleR = this.pidRegulator.handle(this.myPos);
        var throttleS = this.estimator.handle(this.myPos);
        
        var throttle = Math.min(this.fastness, throttleR, throttleS);
        console.log("Lane = ", this.lane);
        console.log("Throttle = ", throttle.toFixed(2)); 
        
        var msgs = [];
        if (this.turbo && this.estimator.shouldTurbo(this.myPos)) {
            console.log("Should turbo.");
            this.turbo = null;
            return {msgType: "turbo", data: "lol"};
        }
        var lanesS = this.estimator.getLanesNext(this.myPos);        
        var avoidLanes = this.overtaker.getAvoidedLanes();
        if ((lanesS.startLaneIndex != lanesS.endLaneIndex) && 
            (lanesS.endLaneIndex != this.lane)) {
            
            if (!~avoidLanes.indexOf(lanesS.endLaneIndex)) {
                console.log("Allowed to switch lanes");
                this.lane = lanesS.endLaneIndex;
                return this.getSwitchDirectionMsg(lanesS);
            } else {
                console.log("Forbidden to switch lanes because");
                console.log(avoidLanes, "contains", lanesS.endLaneIndex,"=",
                    !!~avoidLanes.indexOf(lanesS.endLaneIndex));
                
            }
        }
        else {
            var suggestion = this.overtaker.switchSuggestion(this.myPos);
            if (suggestion !== this.myPos.piecePosition.lane.endLaneIndex && 
                suggestion !== this.lane) {
                var start = this.lane;
                this.lane = suggestion;
                return this.getSwitchDirectionMsg({
                    startLaneIndex: start,
                    endLaneIndex: this.lane
                });
            }
        }
               
        return {msgType: "throttle", data: throttle};
        

    }
    getSwitchDirectionMsg(lanes: PieceLanes) {
        console.log("Switching", lanes);
        var dir = this.track.lanes[lanes.endLaneIndex].distanceFromCenter
                - this.track.lanes[lanes.startLaneIndex].distanceFromCenter;
        var dirStr = dir > 0 ? "Right" : "Left";
        console.log("Switching with msg", {msgType: "switchLane", data: dirStr});
        return {msgType: "switchLane", data: dirStr};
    }

}

class Overtaker {
    private bumps: number[];
    constructor(private t: Track, private cars: Car[]) {
        this.bumps = cars.map(c => 0);
    }
    
    private others: CarPosition[];
    private oldOthers: CarPosition[];
    private speeds: number[];
    
    getRadiusLane(piece: Piece, l: number) {
        var rModifier = this.t.lanes[l].distanceFromCenter;
        if (piece.angle > 0)
            return piece.radius - rModifier;
        else if (piece.angle < 0)
            return piece.radius + rModifier;
        else
            return piece.radius;
    }
    getLength(piece: Piece, lane: number) {
        //todo estimate based on lane.
        return piece.length || this.getRadiusLane(piece, lane)
            * Math.abs(piece.angle) * Math.PI  / 180.0;
    }
    getDistance(pos: CarPosition, pos1: CarPosition) {
        
        if (pos.piecePosition.pieceIndex == pos1.piecePosition.pieceIndex)
            return pos.piecePosition.inPieceDistance
            - pos1.piecePosition.inPieceDistance;
        else {
            var oldPiece = this.t.pieces[pos1.piecePosition.pieceIndex];
            return (this.getLength(oldPiece, pos1.piecePosition.lane.endLaneIndex) 
                - pos1.piecePosition.inPieceDistance) 
                + pos.piecePosition.inPieceDistance;
        }
    }
    private oldPos;
    private mySpeed = 0;
    
    private avoidLanes:number[] = [];
    public stateUpdate(myPos: CarPosition, others: CarPosition[]) {
        others = others.filter(cp => cp.id.color != myPos.id.color);
        this.others = others;
        if (this.oldOthers) {
            this.speeds = this.others.map((o, i) => this.getDistance(o, this.oldOthers[i]));
            this.mySpeed = this.getDistance(myPos, this.oldPos);
                
            this.avoidLanes = [];
            
            others.forEach((other, k) => {
                var otherMinusMy = other.piecePosition.pieceIndex - myPos.piecePosition.pieceIndex;
                if (otherMinusMy > 2 || otherMinusMy < 0)
                    return;
                if (this.speeds[k] / this.mySpeed < 0.9)
                    this.avoidLanes.push(other.piecePosition.lane.endLaneIndex);
                    
            });
        }
        this.oldOthers = this.others;
        this.oldPos = myPos;
        
    }
    switchSuggestion(myPos: CarPosition) {
        
        var myLane = myPos.piecePosition.lane.endLaneIndex,
            suggestions = [myLane];
        var nextItem = (myPos.piecePosition.pieceIndex + 1) % this.t.pieces.length;
            if (!this.t.pieces[nextItem].switch) return myLane;
        
        if (myLane > 0) 
            suggestions.push(myLane - 1)
        if (myLane < this.t.lanes.length - 1)
            suggestions.push(myLane + 1);
        
        var okay = suggestions.filter(alt => this.avoidLanes.indexOf(alt) < 0);
        if (okay.indexOf(myLane)) 
                return myLane;
        if (!okay.length) return myLane;
        
        return okay[0];
    }
    public getAvoidedLanes() {
        return this.avoidLanes;
    }
}

// The only remaining idea I have is to turn of Kd if
// the next turn is at approximately the wrong distance.

class PidRegulator {
    private previousError = null;
    private integral = 0;
    private setPoint = 45;
    private Kp = 1;
    private Ki = 0;
    private Kdp = 10; // 10 is good with latest
    private Kdn = 6; // 5 is good with latest
    
    
    constructor(private t:Track) { }
    public handle(pos:CarPosition) {
        
        var p = this.t.pieces[pos.piecePosition.pieceIndex];
        
        //var offset = 15.0 * Math.max(0, (100 - (p.radius || 100)) / 50) // great on *        
        this.setPoint = 15 + (p.radius || 200) / 4.5; // crashes on usa, great on *
        //this.setPoint = Math.sqrt(p.radius || 200) * 4.5;  // again crash on usa
        //this.setPoint = 12.5 + Math.sqrt(p.radius || 200) * 3;  // 
        this.setPoint = Math.sqrt(p.radius || 200) * 3.8;  // 3.9 worked fine
        
        var angle = pos.angle;
        var error = (this.setPoint - Math.abs(angle)) / 30.0; // try 28 for margin of safety
        if (this.previousError == null)
            this.previousError = error;
        this.integral = this.integral + error;
        if (this.integral > 0)
            this.integral = 0;
        // Since the error goes negative when the angle grows
        // that means the derivative is smaller - larger < 0
        // therefore Kd should be positive
        var derivative = error - this.previousError;
        
        var Kd = derivative > 0 ? this.Kdp : this.Kdn;
        var output = 0.5 + this.Kp * error + this.Ki * this.integral + derivative * Kd;
        this.previousError = error;
        console.log("A=", angle.toFixed(1), "E=", error.toFixed(2), "O=", output.toFixed(2));
        
        return Math.min(1, Math.max(0, output));
    }
}

class Estimator {
    private oldSpeed: number = 0;
    private oldPos: CarPosition;
    private oldAngle = 0;

    private v1: number = 0;
    private v2: number = 0;
    private v3: number = 0;
    private m: number = 0;
    private k: number = 0;
    
    private pickedLanes: PieceLanes[];
    
    constructor(private t:Track) {
        this.pickedLanes = t.pieces.map(p => ({startLaneIndex: 0, endLaneIndex:0}));
    }
    
    pickLanes(pos: CarPosition) {
        var currentLaneIndex = pos.piecePosition.lane.startLaneIndex;
        var currentLane = this.t.lanes[currentLaneIndex];
        
        
        this.takePieceWhileF(pos.piecePosition.pieceIndex, p => {
            this.pickedLanes[p.index].startLaneIndex = currentLaneIndex;
            if (!p.switch) {
                this.pickedLanes[p.index].endLaneIndex = currentLaneIndex;
            } else {
                var laneOptions:number[] = [];
                if (currentLaneIndex != 0)
                    laneOptions.push(currentLaneIndex - 1)
                laneOptions.push(currentLaneIndex);
                if (currentLaneIndex < this.t.lanes.length - 1)
                    laneOptions.push(currentLaneIndex + 1)
                
                var choices = laneOptions.map(lane => ({
                    lane: lane, 
                    len: this.takePieceWhileF(p.index, np => np.index == p.index || !np.switch).items
                        .map(p => this.getLengthLane(p, lane))
                        .reduce<number>((l, acc) => l + acc, 0) 
                }));
                var best = choices.sort((left, right) => left.len - right.len)[0].lane;
                this.pickedLanes[p.index].endLaneIndex = best;
                currentLaneIndex = best;
            }
            if (this.nextPid(p.index) == pos.piecePosition.pieceIndex) 
                return false;
            return true;
        });
    }
    
    shouldTurbo(pos: CarPosition) {
        var total = this
            .takePieceWhileF(pos.piecePosition.pieceIndex, p => !p.angle && !p.radius)
        .items.map(p => p.length)
        .reduce<number>((acc, el) => acc + el, 0);
        console.log("Straight ahead", total);
        if (total > 450) return true;
        return false;
    }
    
    public getLanesNext(pos:CarPosition) {
        return this.pickedLanes[this.nextPid(pos.piecePosition.pieceIndex)];
    }
    
    takePieceWhile(pId: number, direction: (number) => number, predicate: (p:Piece) => boolean) {
        var piece = this.t.pieces[pId], all:Piece[] = [];
        while (predicate(piece)) {
            all.push(piece);
            pId = direction(pId);
            piece = this.t.pieces[pId];
        }
        return {nextPid: pId, items: all};
    }
    
    takePieceWhileF(pId: number, predicate: (p:Piece) => boolean) {
        return this.takePieceWhile(pId, pid => this.nextPid(pid), predicate);
    }
    takePieceWhileR(pId: number, predicate: (p:Piece) => boolean) {
        return this.takePieceWhile(pId, pid => this.prevPid(pid), predicate);
    }
    
    getTurnDirection(piece: Piece) {
        if (!piece.angle) return 0;
        else return piece.angle > 0 ? 1 : -1;
    }
    
    getRadiusLane(piece: Piece, l: number) {
        var rModifier = this.t.lanes[l].distanceFromCenter;
        if (piece.angle > 0)
            return piece.radius - rModifier;
        else if (piece.angle < 0)
            return piece.radius + rModifier;
        else
            return piece.radius;
    }
    getRadius(piece: Piece) {
        var lanes = this.pickedLanes[piece.index];
        return Math.min(this.getRadiusLane(piece, lanes.startLaneIndex),
            this.getRadiusLane(piece, lanes.endLaneIndex));
    }    
    getLengthLane(piece: Piece, l: number) {
        //todo estimate based on lane.
        return piece.length || this.getRadiusLane(piece, l)
            * Math.abs(piece.angle) * Math.PI  / 180.0;
    }
    getLength(piece: Piece) {
        //todo estimate based on lane.
        return piece.length || this.getRadius(piece)
            * Math.abs(piece.angle) * Math.PI  / 180.0;
    }
    nextPid(pid: number) {
        var next = (pid + 1);
        if (next >= this.t.pieces.length)
            return 0;
        return next;
    }
    prevPid(pid: number) {
        var prev = (pid - 1)
        if (prev < 0)
            return this.t.pieces.length - 1;
        return prev;
    }
    percentualDifference(n1, n2) {
        return Math.max(n1 , n2) / Math.min(n1, n2) - 1;
    }
    getAvailableBreakingDistance(pos: CarPosition) {
        var pid = pos.piecePosition.pieceIndex,
            piece = this.t.pieces[pid];
        var len = this.getLength(piece);
        var startTD = this.getTurnDirection(piece),
            startRadius = this.getRadius(piece);
        var thisRemaining = len - pos.piecePosition.inPieceDistance;
        var nextPid = this.nextPid(pid);
        var currentPieces = this.takePieceWhileF(nextPid,
            pc => startTD && this.getTurnDirection(pc) == startTD
            && this.percentualDifference(pc.radius, startRadius) < 0.1);

        var straightPieces = this.takePieceWhileF( 
            currentPieces.nextPid, pc => !this.getTurnDirection(pc));
        
        //console.log("Pieces to next turn: ", currentPieces.items, straightPieces.items);
        
        var endPid = straightPieces.nextPid;
        var reducer = (l, piece) => l + this.getLength(piece);
        var curLen = currentPieces.items.reduce<number>(reducer, 0);
        var straightLen = straightPieces.items.reduce<number>(reducer, 0);
        return {nextPid: endPid, length: thisRemaining + curLen  + straightLen };
    }
    estimateDesiredSpeed(pid: number, speed: number) {
        //var MAXLEN = 250; // this value worked REALLY well. with 0.6
        //var K = 0.65; // this worked really well with 200
        
        var MAXLEN = 160; // 
        var K = 0.66;// * 4.9 / this.m; // tune this;
        
        var piece = this.t.pieces[pid];
        var turnDirection = this.getTurnDirection(piece);
        var d = 0.0, turnAmount = 0.0, angleAmount = 0.0;
        var turnPieces = this.takePieceWhileF(pid, piece => {
            var pLen = this.getLength(piece);

            var take = MAXLEN - d;
            if (take > pLen) take = pLen;
            if (piece.angle) {
                var anglePerc = (Math.abs(piece.angle) * take / pLen);
                angleAmount += anglePerc;
                turnAmount += anglePerc / Math.sqrt(this.getRadius(piece));
            }
            if ((d += pLen) > MAXLEN) return false;
            return true;
        });
        //console.log("Turn pieces", turnPieces);
        
        return K / (turnAmount / angleAmount);

    }
    getDistance(pos: CarPosition) {
        if (!this.oldPos) return 0;
        if (pos.piecePosition.pieceIndex == this.oldPos.piecePosition.pieceIndex)
            return this.oldSpeed = pos.piecePosition.inPieceDistance
            - this.oldPos.piecePosition.inPieceDistance;
        else {
            var oldPiece = this.t.pieces[this.oldPos.piecePosition.pieceIndex];
            return (this.getLength(oldPiece) - this.oldPos.piecePosition.inPieceDistance) 
                + pos.piecePosition.inPieceDistance;
        }
    }
    private setSpeeds(speed: number) {
        if (!this.v1)
            this.v1 = speed;
        else if (!this.v2)
            this.v2 = speed;
        else if (!this.v3) {
            this.v3 = speed;
            // k = ( v(1) - ( v(2) - v(1) ) ) / v(1)2 * h;
            this.k = (this.v1 - (this.v2 - this.v1)) / (this.v1 * this.v1);
            this.m = 1.0 / ( Math.log( ( this.v3 - ( 1 / this.k ) ) / ( this.v2 - (1 / this.k ) ) ) / ( 0 - this.k ) )
            console.log("===");
            console.log("Speed", this.v1.toFixed(3));
            console.log("K=", this.k.toFixed(3), "M=", this.m.toFixed(3));
            console.log("===");
        }
    }
    
    private getNecessaryBreakingTicks(speed:number, desiredSpeed:number) {
        return Math.log(desiredSpeed / speed) * this.m / (0 - this.k)
    }
    private getBreakingDistance(speed: number, ticks: number) {
        
        return (this.m / this.k) * speed * (1.0 - Math.exp((0 - this.k) * ticks / this.m));
    }
    public handle(pos: CarPosition) {
        if (!this.oldPos) {
            this.oldPos = pos;
            return 1;
        }        
        var speed = this.getDistance(pos);
        this.setSpeeds(speed);
        this.oldPos = pos;
        if (!this.m) return 1;
        
        console.log("At", this.t.pieces[pos.piecePosition.pieceIndex]);
        this.pickLanes(pos);
        var availableDistance = this.getAvailableBreakingDistance(pos);
        var desiredSpeed = this.estimateDesiredSpeed(availableDistance.nextPid, speed);
        var necessaryFrames = this.getNecessaryBreakingTicks(speed, desiredSpeed);
        var necessaryDistance = this.getBreakingDistance(speed, necessaryFrames);
        
        console.log("Speed", speed.toFixed(2), "Desired", desiredSpeed.toFixed(2),
                    "BreakDistanceAvailable", availableDistance.length.toFixed(1), 
                    "BreakDistanceNecessary", necessaryDistance.toFixed(1));
        
        if (necessaryDistance > availableDistance.length)
            return 0;
        else
            return 1;
        
        var availableFrames = availableDistance.length / speed;
        
        var toTheEnd = 0.15 * availableFrames; // 0.15 per frame change
        
        console.log("Speed", speed.toFixed(2), "Desired", desiredSpeed.toFixed(2),
                    "AvailableFrames", availableFrames.toFixed(1))
        
        if (speed - desiredSpeed > Math.max(toTheEnd, 0))
            return 0;
        else
            return 1;
        return 0.5;
    }
}


var ping = {msgType: "ping", data: {}};

function create(fastness: number) {
    var handler = new Handler(fastness);
    return data => {
        if (handler[data.msgType])
            return handler[data.msgType](data.data) || ping;
        else {
            console.log("Server:", data);
            return ping;
        }
        
    }
}

export = create;
